import axios from "axios";
import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";

function Edit() {
  const [details, setDetails] = useState([]);
  const [loading, setLoading] = useState(false);
  const params = useParams();
  const navigate = useNavigate();
    console.log(params)
  const formik = useFormik({
    initialValues: {
      featuresname: "",
      Version: "",
    },
    validate: (values) => {
      let errors = {};
      if (values.featuresname === "") {
        errors.featuresname = "Please enter Name ";
      }

      if (values.featuresname.length <= 3) {
        errors.featuresname = "Please enter Name more than 3 letters";
      }
      if (values.Version === "") {
        errors.Version = "Please enter Version";
      }
      return errors;
    },
    onSubmit: async (values) => {
      setLoading(true);
      await axios.put(
        `https://62ab049e371180affbdf40f1.mockapi.io/student/${params.id}`,
        values
      );

      alert("Details Edited Sucessfully...");
      navigate("/");
      setLoading(false);
    },
  });

  useEffect(() => {
    loadUser();
  }, []);

  let loadUser = async () => {
    setLoading(true);
    try {
      let details = await axios.get(
        `https://62ab049e371180affbdf40f1.mockapi.io/student/${params.id}`
      );
      formik.setValues({
        featuresname: details.data.featuresname,
        Version: details.data.Version
      });
      setLoading(false);
    } 
    catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {loading ? (
        <div className="loader"> </div>
      ) : (
        <div className="container">
          <div className="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 className="h3 mb-0 text-dark-800">Edit</h1>
          </div>
          <Link
            to="/"
            className="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
          >
            View Details
          </Link>
          <form onSubmit={formik.handleSubmit}>
            <div className="row">
              <div className="col-lg-12">
                <label>
                  <b>Features Name</b> <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  className={`form-control ${
                    formik.errors.name ? `input-error` : ``
                  }`}
                  type={"text"}
                  value={formik.values.featuresname}
                  onChange={formik.handleChange}
                  placeholder="Enter features name"
                  name="featuresname"
                />
                <span style={{ color: "red" }}>{formik.errors.name}</span>
              </div>

              <div className="col-lg-12">
                <label>
                  {" "}
                  <b>Version</b> <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  className={`form-control ${
                    formik.errors.Version ? `input-error` : ``
                  }`}
                  type={"text"}
                  value={formik.values.Version}
                  onChange={formik.handleChange}
                  name="Version"
                  placeholder="Enter Version"
                />
                <span style={{ color: "red" }}>{formik.errors.Version}</span>
              </div>

              <div className="col-lg-12 mt-2">
                <input
                  className="btn-primary"
                  type={"submit"}
                  value="Edit"
                  disabled={!formik.isValid}
                />
              </div>
            </div>
          </form>
        </div>
      )}
    </>
  );
}
export default Edit;
