import Button from "@mui/material/Button";
import "../Components/Mainpage.css";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import axios from "axios";
import { Link } from "react-router-dom";
import Popover from "@mui/material/Popover";

import React, { useEffect, useState } from "react";

 


export default function Mainpage() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const [details, setDetails] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadData();
  }, []);

  let loadData = async () => {
    let details = await axios.get(
        "http://192.168.29.172:8000/user/retriveTestCase"
      // "https://62ab049e371180affbdf40f1.mockapi.io/student"
    );
    console.log(details);
    setDetails(details.data);
  };

  return (
    <>
      <div className="CreateTestCase">
        <Button
          variant="contained"
          className="createbtn"
          aria-describedby={id}
          onClick={handleClick}
          // href="/"
          // endIcon={<AddCircleIcon />}
        >
          Create TestCase
        </Button>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
        >
          <Button sx={{ p: 1 }} href="/create">
            Add <AddCircleIcon />
          </Button>
        </Popover>
      </div>
      <table className="table table-striped">
        <thead Name="tabalign">
          <tr>
            <th scope="col">S.No</th>
            <th scope="col">Features Name</th>
            <th scope="col">Version</th>
            <th scope="col">Edit</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {details.map((details, index) => {
            return (
              <tr>
                <th scope="row">{index + 1}</th>
                <td>
                  <b>{details.featuresname}</b>
                </td>
                <td>
                  <b>{details.Version}</b>
                </td>
                <td>
                  <Link
                    to={`/edit/${details.id}`}
                    className="btn btn-sm btn-primary mr-2"
                  >
                    Edit
                  </Link>
                </td>
                <td>
                  <Link
                    to={`/edit/${details.id}`}
                    className="btn btn-sm btn-primary mr-2"
                  >
                    Execute
                  </Link>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
