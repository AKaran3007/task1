import React from 'react'

function Logo() {
  return (
    <>
        <img src="https://www.digi-val.com/assets/img/logo.svg" alt="Digival IT Solutions Logo" class="img-fluid" className='img' />
    </>
  )
}

export default Logo
