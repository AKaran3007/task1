import { useFormik } from "formik";
import { useNavigate, Link } from "react-router-dom";
import axios from "axios";
import { useState, React, useEffect } from "react";
import { RecordRTCPromisesHandler } from "recordrtc";

function Create() {
  const [course, setCourse] = useState("");
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState([]);
  console.log(details)
  //record 
  // const [recorder, setRecorder] = useState(null);
  // const [stream, setStream] = useState(null);
  // const [videoBlob, setVideoUrlBlob] = useState(null);
  // console.log("CREATE PAGE","recorder", recorder, stream, "stream", videoBlob , "videoBlob")
  // const [type, setType] = useState("screen");
  //record 

  const handleChange = (e) => {
    setCourse(e.target.value);
  };

  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      featuresname: "",
      version: "",
    },
    validate: (values) => {
      let errors = {};

      if (values.featuresname.length <= 3) {
        errors.featuresname = "Please enter Name more than 3 letters";
      }
      if (values.version === "") {
        errors.version = "Please enter Version";
      }
      return errors;
    },
  });

  const [file, setFile] = useState();

  const send = () => {
    const newFormData = new FormData();
    const upload = {
      version: formik.values.version,
      featuresname: formik.values.featuresname,
    };
    newFormData.append("upload", file);
    // console.log(upload, "upload", formik.values.version);
    // console.log("consoee", upload, file);
    axios
      .post(`http://192.168.29.172:8000/user/testcaseinfo`, upload)
      .then((res) => {
        console.log(res.data, "res");
        newFormData.append("id", res.data);
        axios
          .post(`http://192.168.29.172:8000/user/uploadfile`, newFormData)
          .then((res) => console.log(res, "res"))
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));

    alert("Details Added Sucessfully...");
    // navigate("/");
    window.location.reload()
  };
  useEffect(() => {
    loadData();
  }, []);

  let loadData = async () => {
    setLoading(true);
    let details = await axios.get(
      "http://192.168.29.172:8000/user/retriveTestCase"
      // "https://62ab049e371180affbdf40f1.mockapi.io/student"
    );
    // console.log(details);
    setDetails(details.data);
    setLoading(false);
  };

  //Record starts
  // const startRecording = async () => {
  //   const mediaDevices = navigator.mediaDevices;
  //   const stream =
  //     type === "video"
  //       ? await mediaDevices.getUserMedia({
  //           video: true,
  //           audio: true,
  //         })
  //       : await mediaDevices.getDisplayMedia({
  //           video: true,
  //           audio: false,
  //         });
  //   const recorder = new RecordRTCPromisesHandler(stream, {
  //     type: "video",
  //   });

  //   await recorder.startRecording();
  //   setRecorder(recorder);
  //   setStream(stream);
  //   setVideoUrlBlob(null);
  // };

  // const stopRecording = async () => {
  //   if ((videoBlob, recorder)) {
  //     await recorder.stopRecording();
  //     const blob = await recorder.getBlob();
  //     stream.stop();
  //     setVideoUrlBlob(blob);
  //     setStream(null);
  //     setRecorder(null);
  //     const file = new FormData();
  //     const mp4File = new File([blob], "demo.mp4", { type: "video/mp4" });
  //     file.append("file", mp4File);
  //     console.log("consoee", file, mp4File);
  //     axios
  //       .post(`http://192.168.29.172:8000/user/uploadvideo`, file)
  //       .then((res) => console.log(res, "res"))
  //       .catch((err) => console.log(err));
  //     alert("Details Created Sucessfully...");
  //     navigate("/");
  //   }
  // };

  // const changeType = () => {
  //   if (type === "screen") {
  //     setType("screen");
  //   } else {
  //     setType("video");
  //   }
  // };
  //Record ends
 
  const handleExecute = (details) => {
    console.log(details)
  }
  return (
    <>
      {loading ? (
        <div className="loader"> </div>
      ) : (
        <div className="container">
          <div className="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 className="h3 mb-0 text-dark-800">Create TestCase</h1>
          </div>
          <Link
            to="/"
            className="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
          >
            View Details
          </Link>
          <form onSubmit={formik.handleSubmit}>
            <div className="row">
              <div className="col-lg-12">
                <label>
                  <b>Features Name</b> <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  className={`form-control ${
                    formik.errors.featuresname ? `input-error` : ``
                  }`}
                  type={"text"}
                  value={formik.values.featuresname}
                  onChange={formik.handleChange}
                  placeholder="Enter features name"
                  name="featuresname"
                />
                <span style={{ color: "red" }}>
                  {formik.errors.featuresname}
                </span>
              </div>
              <div className="col-lg-12">
                <label>
                  {" "}
                  <b>Version</b> <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  className={`form-control ${
                    formik.errors.version ? `input-error` : ``
                  }`}
                  type={"text"}
                  value={formik.values.version}
                  onChange={formik.handleChange}
                  name="version"
                  placeholder="Enter Version"
                />
                <span style={{ color: "red" }}>{formik.errors.Version}</span>
              </div>
              <label htmlFor="file">
                <b>File</b> <span style={{ color: "red" }}>*</span>
              </label>{" "}
              <br />
              <input
                className="field"
                type="file"
                id="file"
                accept="pdf,excel"
                onChange={(event) => {
                  setFile(event.target.files[0]);
                }}
              />
              <span style={{ color: "red" }}>{formik.errors.file}</span>
            </div>
            <br />

            <button
              className="btn-primary"
              type={"submit"}
              value="Create"
              disabled={!formik.isValid}
              onClick={() => send()}
            >
              Send
            </button>
          </form>
        </div>
      )}

      <table className="table table-striped">
        <thead Name="tabalign">
          <tr>
            <th scope="col">S.No</th>
            <th scope="col">TestCase ID</th>
            <th scope="col">Status</th>
            <th scope="col">Video</th>
            <th scope="col">Features Name</th>
            <th scope="col">Version</th>
            <th scope="col">Testcase Description</th>
            <th scope="col">StepName</th>
            <th scope="col">Steps</th>
            <th scope="col">createdAt</th>
            <th scope="col">updated At</th>
            <th scope="col">Actual Result</th>
            <th scope="col">Expected Result</th>
            <th scope="col">Priority</th>
            <th scope="col">ID</th>
          </tr>
        </thead>
        <tbody>
          {details.map((details, index) => {
            return (
              <tr>
                <th scope="row">{index + 1}</th>
                <td>
                  <b>{details.TestCaseID}</b>
                </td>
                <td>
                  <Link
                    // to={`/edit/${details.id}`}
                    to={`/execute/recorder`}
                    className="btn btn-sm btn-primary mr-2"
                    // onClick={startRecording}
                    onClick={handleExecute(details)}
                  >
                    Execute
                  </Link>
                </td>
                <td>
                  <b>{"Link"}</b>
                </td>
                <td>
                  <b>{details.featuresname}</b>
                </td>
                <td>
                  <b>{details.version}</b>
                </td>
                <td>
                  <b>{details.TestcaseDescription}</b>
                </td>
                <td>
                  <b>{details.StepName}</b>
                </td>
                <td>
                  <b>{details.Steps}</b>
                </td>
                <td>
                  <b>{details.createdAt}</b>
                </td>
                <td>
                  <b>{details.updatedAt}</b>
                </td>
                <td>
                  <b>{details.ActualResult}</b>
                </td>
                <td>
                  <b>{details.ExpectedResult}</b>
                </td>
                <td>
                  <b>{details.Priority}</b>
                </td>
                
                
                {/* <td>
                  <Link
                    to={`/edit/${details.id}`}
                    className="btn btn-sm btn-primary mr-2"
                  >
                    Edit
                  </Link>
                </td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
export default Create;
