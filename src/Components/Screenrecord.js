import { ReactMediaRecorder } from "react-media-recorder";

import { useReactMediaRecorder } from "react-media-recorder";

const Screenrecord = () => {
  const { status, startRecording, stopRecording, mediaBlobUrl } =
    useReactMediaRecorder({ screen: true });
  console.log(stopRecording);

  return (
    <div>
      <p>{status}</p>
      <button onClick={() => startRecording()}>Start Recording</button>
      <button onClick={() => stopRecording()}>Stop Recording</button>
      <video src={mediaBlobUrl} controls autoPlay loop />
    </div>
  );
};

export default Screenrecord;

// import React from 'react';
// import ReactMediaRecorder from 'react-media-recorder';

// const VideoRecorderComponents = () => {

//   const blobToBase64 = (blob) => {
//     return new Promise((resolve, _) => {
//       const blobURL = URL.createObjectURL(blob);
//       const file = new File([blob], 'self-record.mp4', {
//         type: 'video/mp4',
//         lastModified: Date.now(),
//       });
//     });
//   };
//   return (
//     <div>
//       <ReactMediaRecorder
//         onRecordingComplete={(videoBlob) => blobToBase64(videoBlob)}
//         onStartRecording={() => setBlobURL('')}
//       />
//     </div>
//   );
// };

// export default VideoRecorderComponents
