import React, { useState, useEffect } from "react";
import { RecordRTCPromisesHandler } from "recordrtc";
import axios from "axios";
import { useNavigate, Link } from "react-router-dom";

import { saveAs } from "file-saver";

const MainRecorder = () => {

  useEffect(() => {
    console.log("useEffect")
    startRecording()
  }, []);

  const navigate = useNavigate();
  const [recorder, setRecorder] = useState(null);
  const [stream, setStream] = useState(null);
  const [videoBlob, setVideoUrlBlob] = useState(null);
  console.log("RECORDER PAGE","recorder", recorder, stream, "stream", videoBlob , "videoBlob")
  const [type, setType] = useState("screen");

  const startRecording = async () => {
    const mediaDevices = navigator.mediaDevices;
    const stream =
      type === "video"
        ? await mediaDevices.getUserMedia({
            video: true,
            audio: true,
          })
        : await mediaDevices.getDisplayMedia({
            video: true,
            audio: false,
          });
    const recorder = new RecordRTCPromisesHandler(stream, {
      type: "video",
    });

    await recorder.startRecording();
    setRecorder(recorder);
    setStream(stream);
    setVideoUrlBlob(null);
  };


  const stopRecording = async () => {

   

    if ((videoBlob, recorder)) {
      await recorder.stopRecording();
      const blob = await recorder.getBlob();
      stream.stop();
      setVideoUrlBlob(blob);
      setStream(null);
      setRecorder(null);
      const file = new FormData();
      const mp4File = new File([blob], "demo.mp4", { type: "video/mp4" });
      saveAs(mp4File, `Video-${Date.now()}.mp4`);
      file.append("file", mp4File);
      console.log("consoee", file, mp4File);
      axios
        .post(`http://192.168.29.172:8000/user/uploadvideo`, file)
        .then((res) => console.log(res, "res"))
        .catch((err) => console.log(err));
      alert("Details Created Sucessfully...");
      navigate("/");
    }
  };

  const downloadVideo = () => {
    if (videoBlob) {
      const mp4File = new File([videoBlob], "demo.mp4", { type: "video/mp4" });
      saveAs(mp4File, `Video-${Date.now()}.mp4`);
    }
  };

  const changeType = () => {
    if (type === "screen") {
      setType("screen");
    } else {
      setType("video");
    }
  };

  return (
    <div spacing="5" p="5">
      <div
        display="flex"
        justifyContent="center"
        flexDirection={[
          "column", // 0-30em
          "row", // 30em-48em
          "row", // 48em-62em
          "row", // 62em+
        ]}
      >
        <button onClick={startRecording} className="button">
          Start recording
        </button>
        <button onClick={stopRecording} className="button">
          Stop recording
        </button>
      </div>

      <button onClick={downloadVideo} className="button">
        download
      </button>
    </div>
  );
};

export default MainRecorder;
