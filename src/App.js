import "./App.css";
import Mainpage from "./Components/Mainpage";
import Homepage from "./Components/Homepage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Logo from "./Components/Logo";
import Recorder from "./Components/Recorder";
import Edit from "./Components/Edit";
import Create from "./Components/Create";


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        
        <Logo></Logo>
        
   

        <Routes>
          <Route path="/a" element={<Homepage />} />
          <Route path="/" element={<Mainpage />} />
          <Route path="/edit/:id" element={<Edit />} />
          <Route path="/create" element={<Create />} />
          <Route path="/execute/recorder" element={<Recorder />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;


